import face_recognition
import cv2
import numpy as np

# Esta es una demostración de cómo ejecutar el reconocimiento facial en video en vivo desde su cámara web. 
# Es un poco más complicado que el
# otro ejemplo, pero incluye algunos ajustes básicos de rendimiento para hacer que las cosas funcionen mucho más rápido:
# 1. Procese cada fotograma de video a 1/4 de resolución (aunque aún lo muestre a resolución completa) aunque se puede cambiar pero afectara en el rendimiento
# 2. Solo detecta caras en cualquier otro cuadro de video.

# TENGA EN CUENTA: este ejemplo requiere que OpenCV (la biblioteca `cv2`) se instale solo para leer desde su cámara web.
# OpenCV es * not * necesario para usar la biblioteca face_recognition. Solo es necesario si quieres ejecutar este
# demo específica. Si tiene problemas para instalarlo, pruebe cualquiera de las otras demostraciones que no lo requieran.

# Obtener una referencia a la cámara web # 0 (la predeterminada)
video_capture = cv2.VideoCapture(0)

# Cargue una imagen de muestra y aprenda a reconocerla.
samuel_image = face_recognition.load_image_file("imagenes de entrenamiento/Samuel Trias/Samuel.jpg")
samuel_face_encoding = face_recognition.face_encodings(samuel_image)[0]

# Cargue una segunda imagen de muestra y aprenda a reconocerla.
image_2 = face_recognition.load_image_file("imagenes de entrenamiento/Luis Rodriguez/1.jpg")
cara2_face_encoding = face_recognition.face_encodings(image_2)[0]

# Creamos un array en donde guardamos los nombres a los que pertenecen las fotos
known_face_encodings = [
    samuel_face_encoding,
    cara2_face_encoding
]
known_face_names = [
    "Samuel Trias",
    "Luis Rodriguez"
]

# Declaramos algunas variables
face_locations = []
face_encodings = []
face_names = []
process_this_frame = True

while True:
    # Tomamos un cuadro del video
    ret, frame = video_capture.read()

    # Cambiar el tamaño del cuadro de video a 1/4 para un procesamiento de reconocimiento facial más rápido
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

    # Convertir la imagen del color BGR (que usa OpenCV) al color RGB (que usa face_recognition)
    rgb_small_frame = small_frame[:, :, ::-1]

    # Solo procesa cada otro cuadro de video para ahorrar tiempo.
    if process_this_frame:
        # Encuentre todas las caras y codificaciones de caras en el cuadro actual de video
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # Ver si la cara coincide con la (s) cara (s) conocida (s)
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
            name = "Desconocido"

            # # Si se encontró una coincidencia en known_face_encodings, solo use la primera.
            # si es verdadero en los partidos:
            #       first_match_index = matches.index (true)
            #       name = known_face_names[first_match_index]

            # O en su lugar, use la cara conocida con la distancia más pequeña a la nueva cara
            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_face_names[best_match_index]

            face_names.append(name)

    process_this_frame = not process_this_frame


    # Muestra en pantalla los resultados
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Escale las ubicaciones de las caras de la copia de seguridad desde que el cuadro que detectamos se ajustó a tamaño de 1/4
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # Dibujamos un rectangulo en la cara
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # Dibuja una etiqueta con un nombre debajo de la cara.
        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

    # Mostrar la imagen resultante en una ventana
    cv2.imshow('Reconocimieto Facial', frame)

    # declaramos que si se pulsa 'q' en el teclado se cerrara la aplicacion!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Liberamos la camara para que pueda ser utilizada por otra aplicacion
video_capture.release()
cv2.destroyAllWindows()
