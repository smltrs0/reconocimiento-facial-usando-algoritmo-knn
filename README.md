Si nunca tabajaste con python ten en cuenta que en python los espacios en el codigo son tan importantes como los puntos y comas o las llaves en otros lenguajes.
Reconocimiento facial usando algoritmo knn con modelo pre entrenado.

   **Como intalar** 
* Es necesario tener Python instalado.
* Para intalar los modulos solo coloca en consola `"pip install -r requirements.txt"`
* y se instalaran todos los modulos para que funcione el script

**Despues de tener todo lo necesario para ejecutar los scripts  puedes abrir cualquiera de los 2 ejemplos **
*  reconocimiento_por_camaraweb_rapido.py
*  reconocimiento_usando_algoritmo_knn.py

**Estructura de árbol de entrenamiento** train_dir

Esta es la estructura de los dataset que se usan en el algoritmo  `reconocimiento_usando_algoritmo_knn.py`


     Estructura:
        <imagenes de entrenamiento>/
        ├── <persona1>/
        │   ├── <foto1>.jpeg
        │   ├── <foto2>.jpeg
        │   ├── ...
        ├── <persona2>/
        │   ├── <foto1>.jpeg
        │   └── <foto2>.jpeg
        └── ...
        
        

Los 2 algoritmos se basan en la documentacion oficial de el modulo [](https://pypi.org/project/face_recognition/).